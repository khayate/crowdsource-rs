import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthGuardService {
  constructor(
    private userService: UserService,
    private router: Router,
  ) { }

  async canActivate() {
    if (!(await this.userService.user$.pipe(take(1)).toPromise())) {
      this.router.navigate(['/login']);
      return false;
    }
    if (!this.userService.isRelawan) {
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
