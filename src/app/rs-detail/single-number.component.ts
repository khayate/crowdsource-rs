import { Component, Input, Inject, OnInit, OnChanges, ViewChild, ElementRef, AfterContentInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlaceData, History } from '../rs/rs.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from '../util.service';
import { UserService } from '../user.service';

interface CellHistory {
  ago: string;
  value: string;
  uid: string;
}

interface DialogData {
  title: string;
  placeId: string;
  key: string;
  value: string;
  unitLabel?: string;
  history$: Promise<CellHistory[]>;
}

@Component({
  selector: 'app-single-number-card',
  template: `
    <mat-card matRipple [matRippleCentered]="true"
      class="text-center" style="cursor: pointer" [style.background-color]="bgcolor"
      (click)="openDialog(title, unitLabel, placeData, key, placeData.data[key])">
      <div style="margin-bottom: 0px;">
        <h1 class="lead">{{ placeData.data[key] || 0 | number:'1.0':'id' }}<span>{{unitLabel}}</span></h1>
        <span class="text-light">{{ subtitle }}</span>
      </div>
    </mat-card>
`,
  styles: [`
    .lead {
      display:flex;
      flex-flow:column;
      font-size: 2.4rem;
      margin:0;
    }
    .lead span{
      font-size:.8rem;
      color:#ccc;
    }
`]
})
export class SingleNumberCard implements OnInit, OnChanges {
  @Input() placeData: PlaceData;
  @Input() key: string;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() unitLabel: string;

  bgcolor = '';

  constructor(
    public dialog: MatDialog,
    public userService: UserService,
    private utilService: UtilService,
  ) { }

  ngOnChanges() {
    this.bgcolor = this.utilService.getColor(this.placeData.data[`${this.key}_ts`]?.toMillis());
  }

  ngOnInit() {
    this.ngOnChanges();
  }

  static ago(ts: number) {
    let elapsed = (Date.now() - ts) / 1000;
    if (elapsed < 60) return Math.floor(elapsed) + 's lalu';
    elapsed /= 60;
    if (elapsed < 60) return Math.floor(elapsed) + 'm lalu';
    elapsed /= 60;
    if (elapsed < 24) return Math.floor(elapsed) + 'h lalu';
    return `${Math.floor(elapsed / 24)}d ${Math.floor(elapsed % 24)}h lalu`;
  }

  async getHistory$(history: History, key: string) {
    const arr: CellHistory[] = [];
    if (!history || !history[key]) return arr;
    for (let h of history[key].reverse()) {
      arr.push({
        ago: SingleNumberCard.ago(h.ts),
        value: h.value,
        uid: h.uid
      });
      if (arr.length >= 5) break;
    }
    return arr;
  }

  openDialog(title: string, unitLabel: string, placeData: PlaceData, key: string, value: string): void {
    if (!this.userService.isRelawan) {
      console.warn('Only relawan can edit');
      return;
    }

    const dialogRef = this.dialog.open(SingleNumberDialog, {
      maxWidth: '100vh',
      width: this.utilService.isHandset ? '80vh' : '40vh',
      position: { 'bottom': this.utilService.isHandset ? '0px' : '' },
      disableClose: true,
      autoFocus: true,
      data: { title, unitLabel, placeId: placeData.place.place_id, key, value, history$: this.getHistory$(placeData.history, key) }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }
}

@Component({
  selector: 'app-single-number-dialog',
  template: `
    <div mat-dialog-content [formGroup]="form">
      <p>{{ data.title }}</p>
      <mat-form-field>
        <input matInput #input formControlName="value" type="number"
          (keyup.enter)="submit(form.value)" autocomplete="off"
          [disabled]="disabled" cdkFocusInitial>
          <span *ngIf="data.unitLabel" matSuffix style="color:#ccc;margin-left:10px;">{{data.unitLabel}}</span>
      </mat-form-field>
      <app-min-max-validator [control]="form.get('value')" name="Nilai" [maxValue]="MAX2">
      </app-min-max-validator>
    </div>
    <mat-divider style="margin-left:-24px;margin-right:-24px;"></mat-divider>

    <ng-container *ngIf="data.history$ | async as history">
      <ng-container *ngIf="userService.isModerator && history.length > 0">
        <table>
          <caption>Catatan perubahan:</caption>
          <tr><th>Kapan</th><th>Value</th><th>Oleh</th></tr>
          <tr *ngFor="let h of history">
            <td align="right">{{ h.ago }}</td>
            <td align="center">{{ h.value }}</td>
            <td><app-user [uid]="h.uid" (click)="dialogRef.close()"></app-user></td>
          </tr>
        </table>
      </ng-container>
    </ng-container>

    <div mat-dialog-actions style="justify-content:flex-end;margin-top:0px;">
      <mat-spinner [diameter]="25" *ngIf="disabled"></mat-spinner>
      <button [disabled]="disabled" mat-button (click)="dialogRef.close()">Batal</button>
      <button [disabled]="disabled" mat-raised-button color="primary" type="submit"
        (click)="submit(form.value)">Simpan</button>
    </div>
  `,
})
export class SingleNumberDialog implements AfterContentInit {
  @ViewChild('input', { static: false }) inputRef: ElementRef;

  disabled = false;
  form: FormGroup;
  MAX2 = 999999;

  constructor(
    public userService: UserService,
    public dialogRef: MatDialogRef<SingleNumberDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
  ) {
    this.form = this.fb.group({
      value: [data.value, [Validators.min(0), Validators.max(this.MAX2)]]
    });
  }

  ngAfterContentInit() {
    this.cd.detectChanges();
    this.inputRef.nativeElement.select();
  }

  async submit(value) {
    this.disabled = true;
    try {
      const res: any = await this.userService.post(`edit/${this.data.placeId}/${this.data.key}`, value);
      if (res.error) {
        alert(res.error);
      } else {
        this.dialogRef.close(value);
      }
    } catch (e) {
      alert(e.message);
      console.error(e);
    }
    this.disabled = false;
  }
}
