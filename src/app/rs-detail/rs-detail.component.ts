import { Component, Input, OnChanges, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, merge, BehaviorSubject } from 'rxjs';
import { map, filter, switchMap, distinctUntilChanged, shareReplay } from 'rxjs/operators';
import { PlaceData, Attributes, AttributeDetail } from '../rs/rs.component';
import { SingleNumberCard } from './single-number.component';
import { UtilService } from '../util.service';
import { UserService } from '../user.service';
import { Location } from '@angular/common';

interface Details {
  placeData: PlaceData;
  filledLogistic: AttributeDetail[];
  unfilledLogistic: AttributeDetail[];
}

@Component({
  selector: 'app-rs-detail',
  templateUrl: './rs-detail.component.html',
  styleUrls: ['./rs-detail.component.css']
})
export class RsDetailComponent implements OnChanges {
  @Input() placeId: string;

  @ViewChild(SingleNumberCard) singleNumberCard: SingleNumberCard;

  placeChangeTrigger$ = new BehaviorSubject<string>('');
  details$: Observable<Details>;
  attributes = Attributes;
  updatingContact = false;

  constructor(
    private route: ActivatedRoute,
    private firestore: AngularFirestore,
    public utilService: UtilService,
    public userService: UserService,
    public location: Location,
  ) {
    this.details$ =
      merge(
        this.route.paramMap.pipe(map((params: ParamMap) => params.get('id'))),
        this.placeChangeTrigger$
      ).pipe(
        filter<string>(Boolean),
        distinctUntilChanged(),
        switchMap((placeId: string) =>
          this.firestore.collection('rs')
            .doc<PlaceData>(placeId)
            .valueChanges()),
        map((placeData: PlaceData) => {
          const filledLogistic = [];
          const unfilledLogistic = [];
          for (let l of this.attributes.logistic) {
            if (placeData.data[l.key] || l.show) {
              filledLogistic.push(l);
            } else {
              unfilledLogistic.push(l);
            }
          }
          return { placeData, filledLogistic, unfilledLogistic };
        }),
        shareReplay(1)
      );
  }

  ngOnChanges() {
    this.placeChangeTrigger$.next(this.placeId);
  }

  async saveKondisi(placeId: string, value) {
    const res:any = await this.userService.post(`edit/${placeId}/kondisi`, { value });
    if (res.error) {
      alert(res.error);
    }
  }

  async editContact(placeData: PlaceData) {
    const contact = prompt('Masukkan nomor WA orang yang bisa dihubungi untuk permintaan alat-alat di RS/Puskesmas/Faskes ini:', placeData.data.contact);
    if (contact && contact !== placeData.data.contact) {
      this.updatingContact = true;
      try {
        const res: any = await this.userService.post(`edit/${placeData.place.place_id}/contact`, { value: contact });
        if (res.error) {
          alert(res.error);
        }
      } catch (e) {
        alert(e.message);
        console.error(e);
      }
      this.updatingContact = false;
    }
  }
}
