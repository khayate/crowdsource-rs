import { Injectable } from '@angular/core';
import { firestore } from 'firebase/app';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Injectable({ providedIn: 'root' })
export class UtilService {
  // https://simple.wikipedia.org/wiki/Stellar_classification
  SETELLAR_COLOR = [
    '#9db4ff', // 5m blue
    '#aabfff', // 15m deep blue white
    '#cad8ff', // 45m blue white
    '#ffddb4', // 2h pale yellow orange
    '#ffbd6f', // 6h light orange red
    '#f84235', // 20h scarlet
    '#ba3059', // 2d magenta
    '#605170' // dark purple
  ];
  isHandset = false;

  constructor(
    breakpointObserver: BreakpointObserver,
  ) {
    breakpointObserver
      .observe(Breakpoints.Handset)
      .subscribe(result => this.isHandset = result.matches);
  }

  getColor(ts: number | firestore.Timestamp, type: string = 'bg') {
    if (typeof ts !== 'number') {
      ts = ts?.toMillis();
    }
    if (ts) {
      const c = this.SETELLAR_COLOR;
      const ago = (Date.now() - ts) / 1000 / 60;
      for (let i = 0, t = 5; i < c.length; i++, t *= 3) {
        if (ago <= t) {
          if (type == 'bg') return c[i];
          else {
            if (i > 5) return 'inherit';
          }
        }
      }
    }
    return '';
  }
}
