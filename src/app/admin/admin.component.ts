import { Component } from '@angular/core';
import { Relawan, UserService, USER_ROLE } from '../user.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { distinctUntilChanged, debounceTime, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {
  USER_ROLE = USER_ROLE;
  publicUsers$: Observable<Relawan[]>;
  name = '';
  nameFilterTrigger$ = new BehaviorSubject('');

  constructor(
    public userService: UserService,
    private db: AngularFirestore,
  ) {
    this.publicUsers$ =
      this.nameFilterTrigger$.pipe(
        distinctUntilChanged(),
        debounceTime(500),
        switchMap(name => this.db
          .collection<Relawan>('users', (ref: firebase.firestore.Query) => {
            ref = ref
              .orderBy('nameLowerCase', 'asc')
              .orderBy('role', 'asc')
              .orderBy('registerDate', 'desc')
              .limit(25);
            if (name.length > 0) {
              ref = ref
                .startAt(name.toLowerCase())
                .endAt(name.toLowerCase() + '~');
            }
            return ref;
          }).
          valueChanges())
      );

  }
}
