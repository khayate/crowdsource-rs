import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { USER_ROLE, Relawan } from '../user.service';
import { PlaceData } from '../rs/rs.component';
import { UserService } from '../user.service';

export interface Revision {
  key: string;
  value: string;
  prev: string;
  uid: string;
  ts: Date;
  user: Relawan;
}

@Component({
  selector: 'app-rs-log',
  templateUrl: './rs-log.component.html',
  styleUrls: ['./rs-log.component.css']
})
export class RsLogComponent implements OnInit, OnChanges {
  @Input() placeData: PlaceData;

  USER_ROLE = USER_ROLE;
  changes: Revision[] = [];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.ngOnChanges();
  }

  async ngOnChanges() {
    this.changes = [];
    for (const [key, arr] of Object.entries(this.placeData.history ?? [])) {
      let prev;
      for (const h of arr) {
        this.changes.push({
          key,
          uid: h.uid,
          ts: new Date(h.ts),
          value: h.value,
          prev,
          user: null,
        });
        prev = h.value;
      }
    }
    this.changes.sort((a, b) => b.ts.getTime() - a.ts.getTime()).splice(15);
    for (const c of this.changes) {
      c.user = (await this.userService.getUser(c.uid));
    }
  }
}
