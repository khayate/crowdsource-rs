import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User, UserInfo, auth } from 'firebase/app';
import { Observable } from 'rxjs';
import { retry, shareReplay, switchMap, filter, take } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';
import { HttpClient } from '@angular/common/http';
import { PlaceData } from './rs/rs.component';

export enum USER_ROLE {
  PUBLIC = 0,     // Can view tabulation.
  RELAWAN = 1,    // Can search and edit RS data.
  MODERATOR = 2,  // Can promote PUBLIC -> RELAWAN.
  ADMIN = 3,      // Can promote PUBLIC/RELAWAN -> MODERATOR.
}

export interface Relawan extends UserInfo {
  userid: string;
  nameLowerCase: string;
  role: USER_ROLE;
  registerDate: firestore.FieldValue;
}

@Injectable({ providedIn: 'root' })
export class UserService {
  firebaseUser$: Observable<User>;
  user$: Observable<Relawan>;
  user: Relawan;
  userid = '';
  isSignedIn: boolean;

  // In-memory cache for user details.
  relawanCache: { [uid: string]: Relawan } = {};

  // In-memory cache for rumah sakit.
  rsCache: { [placeId: string]: PlaceData } = {};

  constructor(
    public afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private http: HttpClient,
  ) {
    this.firebaseUser$ = this.afAuth.user.pipe(filter<User>(Boolean), shareReplay(1));
    this.user$ = this.firebaseUser$.pipe(
      switchMap(this.ensureUserExists.bind(this)),
      switchMap((user: User) =>
        this.db.collection('users').doc<Relawan>(user.uid).valueChanges()),
      shareReplay(1));

    this.user$.subscribe(user => {
      if (!user) return;
      this.userid = user.userid;
      this.user = user;
    });

    this.afAuth.getRedirectResult().then(result => {
      this.isSignedIn = !!result.user;
      // Use result.credential.accessToken to call the Facebook API.
    }).catch(error => {
      console.error(error);
    });
  }

  async ensureUserExists(user: User) {
    if (user.isAnonymous) {
      await this.logout();
      return null;
    }
    const uRef = this.db.collection('users').doc<Relawan>(user.uid);
    if ((await uRef.get().pipe(take(1)).toPromise()).data()) {
      return user;
    }
    const newUser: Relawan = {
      userid: user.uid,
      nameLowerCase: user.providerData[0].displayName.toLowerCase(),
      ...user.providerData[0],
      role: USER_ROLE.PUBLIC,
      registerDate: firestore.FieldValue.serverTimestamp()
    };
    console.log('set newUser', newUser);
    await uRef.set(newUser);
    return user;
  }

  get role() {
    return this.user && USER_ROLE[this.user.role];
  }

  get isRelawan() {
    return this.user && this.user.role >= USER_ROLE.RELAWAN;
  }

  get isModerator() {
    return this.user && this.user.role >= USER_ROLE.MODERATOR;
  }

  async loginFacebook() {
    try {
      var provider = new auth.FacebookAuthProvider();
      await this.afAuth.signInWithRedirect(provider);
    } catch (e) {
      alert(e);
    }
  }

  async getUser(uid: string): Promise<Relawan> {
    if (this.relawanCache[uid]) return this.relawanCache[uid];
    await this.user$.pipe(take(1)).toPromise();
    const uRef = this.db.collection<Relawan>('users').doc(uid);
    const res = (await uRef.get().pipe(take(1)).toPromise()).data() as Relawan;
    if (!res) return {} as Relawan;
    return this.relawanCache[uid] = res;
  }

  async getRs(placeId: string): Promise<PlaceData> {
    if (this.rsCache[placeId]) return this.rsCache[placeId];
    const uRef = this.db.collection<PlaceData>('rs').doc(placeId);
    const res = (await uRef.get().pipe(take(1)).toPromise()).data() as PlaceData;
    if (!res) return {} as PlaceData;
    return this.rsCache[placeId] = res;
  }

  async changeRole(user: Relawan, role: USER_ROLE) {
    await this.db.collection('users')
      .doc(user.userid)
      .update({ role, approver: this.userid });
  }

  static API_PREFIX = 'https://us-central1-kawalcovid19.cloudfunctions.net/api';

  async get<T>(path: string) {
    const url = `${UserService.API_PREFIX}/${path}`;
    return this.http
      .get<T>(url, await this.getHeaders())
      .pipe(retry(3), take(1))
      .toPromise();
  }

  async post<T>(path: string, body: any) {
    const url = `${UserService.API_PREFIX}/${path}`;
    return this.http
      .post<T>(url, body, await this.getHeaders())
      .pipe(retry(3), take(1))
      .toPromise();
  }

  private async getHeaders() {
    const user = await this.firebaseUser$.pipe(take(1)).toPromise();
    const idToken = await user.getIdToken();
    const headers = { Authorization: `Bearer ${idToken}` };
    return { headers };
  }

  async logout() {
    await this.afAuth.signOut();
    location.reload();
  }
}
