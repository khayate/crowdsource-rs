import { Component, OnInit } from '@angular/core';
import { UserService, USER_ROLE } from '../user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  USER_ROLE = USER_ROLE;

  constructor(public userService: UserService) {
  }

  ngOnInit(): void {
  }
}
