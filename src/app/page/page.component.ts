import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, filter, tap } from 'rxjs/operators';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
  slug$: Observable<string>;
  pages = ['about','privacy','faq','roles', 'disclaimer'];

  constructor(private route: ActivatedRoute, private router: Router) {
    this.slug$ = this.route.paramMap.pipe(
      map((params: ParamMap) => params.get('slug')),
      filter<string>(Boolean),
      tap((slug) => {
        if(this.pages.indexOf(slug) == -1) this.router.navigate(['/viz']);
      })
    )
  }

  ngOnInit(): void {
  }

}
