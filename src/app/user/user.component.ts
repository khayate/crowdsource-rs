import { Component, OnInit, Input } from '@angular/core';
import { UserService, USER_ROLE, Relawan } from '../user.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map, filter, distinctUntilChanged, switchMap, shareReplay } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserChanges } from '../rs/rs.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user',
  template: `<a [routerLink]="['/u', uid]">{{ user?.displayName }}</a>`,
})
export class UserComponent implements OnInit {
  @Input() uid = '';
  user = {} as Relawan;

  constructor(private userService: UserService) { }

  async ngOnInit() {
    if (!this.uid) return;
    this.user = await this.userService.getUser(this.uid);
  }
}

interface UserDetails {
  user: Relawan;
  changes: UserChanges;
  place: { [placeId: string]: google.maps.places.PlaceResult };
}

@Component({
  selector: 'app-user-details',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserDetailsComponent implements OnInit {
  userDetails$: Observable<UserDetails>;
  USER_ROLE = USER_ROLE;
  Object = Object;

  constructor(
    public userService: UserService,
    private route: ActivatedRoute,
    private firestore: AngularFirestore,
  ) {
    this.userDetails$ =
      this.route.paramMap.pipe(
        map((params: ParamMap) => params.get('id')),
        filter<string>(Boolean),
        distinctUntilChanged(),
        switchMap(uid => this.userService.getUser(uid)),
        switchMap((user: Relawan) =>
          this.firestore.collection('user_changes')
            .doc<UserChanges>(user.userid)
            .valueChanges()
            .pipe(map(changes => ({ user, changes: changes ?? {}, place: {} } as UserDetails)))),
        switchMap(async uc => {
          const arr = await Promise.all(Object.keys(uc.changes).map(placeId => this.userService.getRs(placeId)));
          for (const rs of arr) {
            uc.place[rs.place.place_id] = rs.place;
          }
          return uc;
        }),
        shareReplay(1)
      );
  }

  ngOnInit(): void {
  }
}
