import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VizComponent } from './viz/viz.component';
import { RsAddComponent } from './rs-add/rs-add.component';
import { RegistrationComponent } from './registration/registration.component';
import { AuthGuardService } from './auth-guard.service';
import { AdminComponent } from './admin/admin.component';
import { RsDetailComponent } from './rs-detail/rs-detail.component';
import { ListComponent } from './list/list.component';
import { PageComponent } from './page/page.component';
import { UserDetailsComponent } from './user/user.component';

const routes: Routes = [
  { path: 'rs/:id', component: RsDetailComponent },
  { path: 'rs', component: RsAddComponent, canActivate: [AuthGuardService] },
  { path: 'viz/:id', component: VizComponent },
  { path: 'u/:id', component: UserDetailsComponent },
  { path: 'list', component: ListComponent },
  { path: 'login', component: RegistrationComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'page/:slug', component: PageComponent },
  { path: '**', redirectTo: '/viz/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
