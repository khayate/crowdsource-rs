export enum NUMBER_DATA_KEY {
  // Suspect
  odp = 'odp',
  pdpp = 'pdpp',
  pdpr = 'pdpr',
  pdps = 'pdps',
  covp = 'covp',
  covr = 'covr',
  covs = 'covs',

  // APDs
  maskn95 = 'maskn95',
  maskSurgical = 'maskSurgical',
  gloves = 'gloves',
  glovesSteril = 'glovesSteril',
  cap = 'cap',
  goggle = 'goggle',
  faceShield = 'faceShield',
  surgicalGown = 'surgicalGown',
  hazmats = 'hazmats',
  apron = 'apron',
  boots = 'boots',
  shoeCover = 'shoeCover',
  solida = 'solida',
  handSanitizer = 'handSanitizer',
  handSoap = 'handSoap',
  termometerInfrared = 'termometerInfrared',
  ventilator = 'ventilator',

  // Rooms
  icu = 'icu',
  isolasi = 'isolasi',
  kondisi = 'kondisi',

  // Available Tools | Stock for Isolation Room
  availableVentilator = 'availableVentilator',
  availableMechanicalVentilator = 'availableMechanicalVentilator',
  availableEcmo = 'availableEcmo',

  // Total field with type number.
  count = 'count',
}

export enum STRING_DATA_KEY {
  contact = 'contact'
}

export type NumberDataKeyType = { [key in NUMBER_DATA_KEY]: number };
export type StringDataKeyType = { [key in STRING_DATA_KEY]: string };
export interface Data extends NumberDataKeyType, StringDataKeyType { }

export type NumberHistoryType = { value: number, uid: string, ts: number };
export type StringHistoryType = { value: string, uid: string, ts: number };
export type NumberHistoryDataKeyType = { [key in NUMBER_DATA_KEY]: NumberHistoryType[] };
export type StringHistoryDataKeyType = { [key in STRING_DATA_KEY]: StringHistoryType[] };
export interface History extends NumberHistoryDataKeyType, StringHistoryDataKeyType { }

export interface PlaceData {
  place: google.maps.places.PlaceResult;
  data: Data;
  history: History;
}

export type RsHistory = { [key: string]: { value: string | number, ts: number } }
export type UserChanges = { [placeId: string]: RsHistory };

export interface AttributeDetail {
  key: string;
  label: string;
  name?: string;
  longLabel?: string;
  shortLabel: string;
  formLabel: string;
  isPublic: boolean;
  show?: boolean;
  unitLabel?: string;
}

export const Attributes: { [key: string]: AttributeDetail[] } = {
  suspect: [
    // Suspect
    {
      key: 'odp',
      label: 'ODP',
      longLabel: 'Orang Dalam Pemantauan',
      shortLabel: 'ODP',
      formLabel: 'Jumlah Orang Dalam Pemantauan',
      isPublic: false
    },
    {
      key: 'pdpp',
      label: 'PDP Meninggal',
      longLabel: 'Pasien Dalam Pengawasan yang Meninggal',
      formLabel: 'Jumlah Pasien Dalam Pengawasan yang Meninggal',
      shortLabel: 'PDP+',
      isPublic: false
    },
    {
      key: 'pdpr',
      label: 'PDP Dirawat',
      longLabel: 'Pasien Dalam Pengawasan yang Dirawat',
      formLabel: 'Jumlah Pasien Dalam Pengawasan yang Dirawat',
      shortLabel: 'PDP',
      isPublic: false
    },
    {
      key: 'pdps',
      label: 'PDP Sembuh',
      longLabel: 'Pasien Dalam Pengawasan yang Sembuh',
      formLabel: 'Jumlah Pasien Dalam Pengawasan yang Sembuh',
      shortLabel: 'PDP*',
      isPublic: false
    },
    {
      key: 'covp',
      label: 'Positif Covid-19 Meninggal',
      longLabel: 'Pasien Positif Covid-19 yang Meninggal',
      formLabel: 'Jumlah Pasien Positif Covid-19 yang Meninggal',
      shortLabel: 'Cov+',
      isPublic: false
    },
    {
      key: 'covr',
      label: 'Positif Covid-19 Dirawat',
      longLabel: 'Pasien Positif Covid-19 yang Dirawat',
      formLabel: 'Jumlah Pasien Positif Covid-19 yang Dirawat',
      shortLabel: 'Cov',
      isPublic: false
    },
    {
      key: 'covs',
      label: 'Positif Covid-19 Sembuh',
      longLabel: 'Pasien Positif Covid-19 yang Sembuh',
      formLabel: 'Jumlah Pasien Positif Covid-19 yang Sembuh',
      shortLabel: 'Cov*',
      isPublic: false
    },
  ],
  logistic: [
    // APDs and logistics
    {
      key: 'maskSurgical',
      name: 'Surgical Mask',
      shortLabel: 'R.SM',
      label: 'Kebutuhan Surgical Mask',
      formLabel: 'Jumlah Surgical Mask yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'maskn95',
      name: 'N95 Mask',
      shortLabel: 'R.N95',
      label: 'Kebutuhan N95 Mask',
      formLabel: 'Jumlah N95 Mask yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'gloves',
      name: 'Sarung Tangan',
      shortLabel: 'R.ST',
      label: 'Kebutuhan Sarung Tangan',
      formLabel: 'Jumlah Sarung tangan non-steril yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'glovesSteril',
      name: 'Sarung Tangan Steril',
      shortLabel: 'R.STT',
      label: 'Kebutuhan Sarung Tangan Steril',
      formLabel: 'Jumlah Sarung tangan steril yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'cap',
      name: 'Pelindung Kepala (cap)',
      shortLabel: 'R.Cap',
      label: 'Kebutuhan Pelindung Kepala (cap)',
      formLabel: 'Jumlah Pelindung Kepala (cap) yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'goggle',
      name: 'Goggle',
      shortLabel: 'R.Gog',
      label: 'Kebutuhan Goggle',
      formLabel: 'Jumlah Goggle yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'faceShield',
      name: 'Face Shield',
      shortLabel: 'R.FC',
      label: 'Kebutuhan Face Shield',
      formLabel: 'Jumlah Face Shield yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'surgicalGown',
      name: 'Surgical Gown',
      shortLabel: 'R.SG',
      label: 'Kebutuhan Surgical Gown',
      formLabel: 'Jumlah Surgical Gown yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'hazmats:',
      name: 'Coverall/Baju Hazmat',
      shortLabel: 'R.Haz',
      label: 'Kebutuhan Coverall/Baju Hazmat',
      formLabel: 'Jumlah Coverall/Baju Hazmat yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'apron',
      name: 'Apron',
      shortLabel: 'R.Apr',
      label: 'Kebutuhan Apron',
      formLabel: 'Jumlah Apron yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'boots',
      name: 'Sepatu Boots',
      shortLabel: 'R.Boots',
      label: 'Kebutuhan Sepatu Boots',
      formLabel: 'Jumlah Sepatu Boots yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pasang',
    },
    {
      key: 'shoeCover',
      name: 'Shoe Cover',
      shortLabel: 'R.SC',
      label: 'Kebutuhan Shoe Cover',
      formLabel: 'Jumlah Shoe Cover yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'solida',
      name: 'Solida',
      shortLabel: 'R.Sol',
      label: 'Kebutuhan Solida',
      formLabel: 'Jumlah Solida yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'handSanitizer',
      name: 'Hand Sanitizer',
      shortLabel: 'R.HSan',
      label: 'Kebutuhan Hand Sanitizer',
      formLabel: 'Jumlah Hand Sanitizer yang dibutuhkan',
      isPublic: true,
      unitLabel: 'botol 250cc',
    },
    {
      key: 'handSoap',
      name: 'Sabun Cuci Tangan',
      shortLabel: 'R.HSoap',
      label: 'Kebutuhan Sabun Cuci Tangan',
      formLabel: 'Jumlah sabun cuci tangan yang dibutuhkan',
      isPublic: true,
      unitLabel: 'botol',
    },
    {
      key: 'termometerInfrared',
      name: 'Termometer Infrared',
      shortLabel: 'R.Ter',
      label: 'Kebutuhan Termometer Infrared',
      formLabel: 'Jumlah Termometer Infrared yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'ventilator',
      name: 'Ventilator',
      shortLabel: 'R.Vent',
      label: 'Kebutuhan Ventilator',
      formLabel: 'Jumlah Ventilator yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
  ],
  bed: [
    // Rooms
    {
      key: 'icu',
      shortLabel: 'ICUs',
      label: 'Kapasitas Tempat Tidur Ruang ICU',
      formLabel: 'Kapasitas ruang ICU di rumah sakit ini',
      isPublic: true
    },
    {
      key: 'isolasi',
      shortLabel: 'Isolations',
      label: 'Kapasitas Tempat Tidur Ruang Isolasi',
      formLabel: 'Kapasitas ruang isolasi di rumah sakit ini',
      isPublic: true
    },
    {
      key: 'kondisi',
      shortLabel: 'Overloaded',
      label: 'Ketersediaan Tempat Tidur',
      formLabel: 'Daya tampung tempat tidur/ruangan',
      isPublic: true
    },
  ],
  availibility: [
    // Availibity tools for support intensive care / isolation
    {
      key: 'availableVentilator',
      shortLabel: 'Av.Vent',
      longLabel: 'Ketersediaan Ventilator',
      label: 'Ventilator',
      formLabel: 'Ketersediaan Non Invasive Ventilator di rumah sakit ini',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'availableMechanicalVentilator',
      shortLabel: 'Av.MVent',
      longLabel: 'Ketersediaan Mechanical Ventilator',
      label: 'Mechanical Ventilator',
      formLabel: 'Ketersediaan Mechanical Ventilator di rumah sakit ini',
      isPublic: true,
      unitLabel: 'unit',
    },
    {
      key: 'availableEcmo',
      shortLabel: 'Av.ECMO',
      longLabel: 'Ketersediaan Extracorporeal Membrane Oxygenation (ECMO)',
      label: 'ECMO',
      formLabel: 'Ketersediaan Extracorporeal Membrane Oxygenation (ECMO) di rumah sakit ini',
      isPublic: true,
      unitLabel: 'unit',
    }
  ]
};
