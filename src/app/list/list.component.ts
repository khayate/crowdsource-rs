import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserService } from '../user.service';
import { switchMap, shareReplay, map } from 'rxjs/operators';
import { PlaceData, Attributes } from '../rs/rs.component';
import { Observable } from 'rxjs';
import { UtilService } from '../util.service';

interface KvDetail {
  key: string;
  shortLabel: string;
  value: number;
  unit?: string;
}

interface RsData {
  placeData: PlaceData;
  suspects: KvDetail[];
  logistics: KvDetail[];
  beds: KvDetail[];
  availabilities: KvDetail[];
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {
  allPlaceData$: Observable<RsData[]>;
  Object = Object;

  constructor(
    public utilService: UtilService,
    public userService: UserService,
    private firestore: AngularFirestore,
  ) {
    const groupKey = {}, shortLabel = {}, unitLabel = {};
    for (let a of Attributes.suspect) {
      groupKey[a.key] = 'suspects';
      shortLabel[a.key] = a.name || a.shortLabel;
    }
    for (let a of Attributes.logistic) {
      groupKey[a.key] = 'logistics';
      shortLabel[a.key] = a.name || a.shortLabel;
      unitLabel[a.key] = a.unitLabel;
    }
    for (let a of Attributes.bed) {
      groupKey[a.key] = 'beds';
      shortLabel[a.key] = a.name || a.shortLabel;
    }
    for (let a of Attributes.availibility) {
      groupKey[a.key] = 'availabilities';
      shortLabel[a.key] = a.name || a.shortLabel;
      unitLabel[a.key] = a.unitLabel;
    }

    this.allPlaceData$ =
      this.firestore
        .collection<PlaceData>('rs',
          ref => ref
            .where('place.types', 'array-contains-any', ['hospital', 'health'])
            .where('data.count', '>', 0)
            .orderBy('data.count', 'desc')
        )
        .valueChanges().pipe(
          map((pds: PlaceData[]) => {
            const allData = [];
            for (const pd of pds) {
              const rsData: RsData = { placeData: pd, suspects: [], logistics: [], beds: [], availabilities: [] };
              for (let [k, v] of Object.entries(pd.data)) {
                if (!groupKey[k] || typeof v !== 'number' || v <= 0) continue;
                const arr = rsData[groupKey[k]];
                arr.push({ key: k, shortLabel: shortLabel[k], value: v, unitLabel: unitLabel[k] || ''});
              }
              allData.push(rsData);
            }
            return allData;
          }),
          shareReplay(1));
  }
}
